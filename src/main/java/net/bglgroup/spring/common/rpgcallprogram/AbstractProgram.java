package net.bglgroup.spring.common.rpgcallprogram;

import java.beans.IndexedPropertyDescriptor;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.hamcrest.Description;
import org.springframework.beans.factory.annotation.Autowired;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.data.Descriptor;
import com.ibm.as400.data.PcmlException;
import com.ibm.as400.data.ProgramCallDocument;

import net.bglgroup.spring.common.rpgcallprogram.connectionpool.ConnectionPool;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGErrorResponse;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;

public abstract class AbstractProgram implements Program
{		
	@Autowired
    private ConnectionPool connectionPool;
			
	protected ProgramCallDocument programCallDocument;
	
	protected RPGRequest request;
	
	private String libraryPath = "/QSYS.LIB/%LIBL%.LIB/";
				
	public void setRPGRequest(RPGRequest request)
	{
		this.request = request;
	}
		
	public void createPCMLDocument(RPGRequest request) throws PcmlException, RPGException
	{    	
		programCallDocument = new ProgramCallDocument(request.getPcmlFilePath() + request.getProgramName());		
		programCallDocument.setPath(request.getProgramName(), libraryPath + request.getProgramName() + request.getProgramExtension());
	
	} 
	
	public void loadPCMLDocument(RPGRequest request) throws PcmlException, RPGException
	{
	    // Load Request Headers into PCML Document
	    loadFieldMap(request.getProgramName(), ((HashMap<String, Object>) request.getHeaders()));
	    
	    // Load Request Body into PCML Document
	    loadFieldMap(request.getProgramName(), ((HashMap<String, Object>) request.getBody()));		
	}
	
	public AS400 getAs400Connection(RPGRequest request) throws RPGException
	{
		return connectionPool.getConnection(request);	   
	}
	
	public void setAs400(AS400 as400)
	{
		programCallDocument.setSystem(as400);
	}
	
	public void releaseConnection(RPGRequest request, AS400 as400)
	{
		connectionPool.releaseConnection(request, as400); 
	}
	
	private String getAsciiNullString(int fieldSize)
	{				
        byte[] asciiNulInBytes = new byte[fieldSize];
        Arrays.fill(asciiNulInBytes, (byte)0x00);
        return new String(asciiNulInBytes);	
	}
	
	public void executeProgramCall(RPGRequest request) throws PcmlException, RPGException
	{										
		boolean callSuccess = programCallDocument.callProgram(request.getProgramName());			      
       
        if (!callSuccess) 
        {
        	// Retrieve list of server messages
            AS400Message[] msgs = programCallDocument.getMessageList(request.getProgramName());
            
            // Only throw first error list
            throw new RPGException(request, msgs[0].getID() + " - " + msgs[0].getText());
        }         
	}
	
	public void callProgram(RPGRequest request) throws RPGException
	{				
		AS400 as400 = null;
		
		try 
		{
		    setRPGRequest(request);
		    createPCMLDocument(request);
		    loadPCMLDocument(request);
		
		    as400 = getAs400Connection(request);
		
		    setAs400(as400);
		    executeProgramCall(request);
		    
		    verifyRPGSeverity(request, getHighSeverityCode());

		    
		} catch (PcmlException pcmlException) 
		{
		    throw new RPGException(request, pcmlException.getMessage());
		    
		} catch (RPGException rpgException) 
		{
		    throw rpgException;
		}
		finally 
		{
			if (as400 != null) {
			    releaseConnection(request, as400);
			}
		}
	}
	
	public Object getResponseValue(String fieldName) throws RPGException
	{
		Object fieldValue;
		try 
		{
			fieldValue = programCallDocument.getValue(request.getProgramName() + "." + fieldName);
			fieldValue = (fieldValue.equals("")) ? null : fieldValue;
		}
		catch (PcmlException e) 
	    {
		    throw new RPGException(request, e.getMessage());
	    }
		
		return fieldValue;
		
	}
	
	public Object getArrayResponseValue(String fieldName, int index) throws RPGException
	{       	
		Object fieldValue;
		try 
		{
			fieldValue = programCallDocument.getValue(request.getProgramName() + "." + fieldName, getIndices(index));
			fieldValue = (fieldValue.equals("")) ? null : fieldValue;
		}
		catch (PcmlException e) 
	    {
		    throw new RPGException(request, e.getMessage());
	    }
		
		return fieldValue;
		
	}
	
	public int[] getIndices(int index)
	{
		int indices[] = new int[1];
       	indices[0] = index; 
       	
       	return indices;
	}
	
	protected void loadFieldMap(String prefix, Map<String, Object> fieldMap) throws PcmlException, RPGException
	{
		loadFieldMap(prefix, fieldMap, -1);
	}
	
	@SuppressWarnings("unchecked")
	protected void loadFieldMap(String prefix, Map<String, Object> fieldMap, int index ) throws PcmlException, RPGException
	{	    
		var fieldMapClone = new HashMap<String, Object>();
		fieldMapClone.putAll(fieldMap);
				
		Iterator<Entry<String, Object>> it = fieldMapClone.entrySet().iterator();
	    
	    while (it.hasNext()) 
	    {
	        Map.Entry<String, Object> pair = (Map.Entry<String, Object>)it.next();
	        
	        String nameString = prefix + "." + pair.getKey();
	        
	        if (pair.getValue() instanceof ArrayList)
	        {
				ArrayList<Object> arrayList = (ArrayList<Object>) pair.getValue();
	        	int arrayIndex = 0;
	        	
	        	for (Object object : arrayList)
	        	{
	        		if (object instanceof HashMap)
	        		{
	        			loadFieldMap(nameString, (HashMap<String, Object>) object, arrayIndex++);
	        		}
	        	}
	        }
	        else
	        {
	        	Object value = pair.getValue();	        		     	
	        	
	        	if (value != null)
	        	{
	        		if (index >= 0)
	        		{
		        		programCallDocument.setValue(nameString, getIndices(index), value);
	        		} 
	        		else
	        		{
	        		    programCallDocument.setValue(nameString, value);
	        		}	        		
	        	}
	        	else //Numbers will always be set to zero and arrays will always be set to empty array lists. Only string values should be null.
	        	{
	        		programCallDocument.setValue(nameString, getAsciiNullString(programCallDocument.getOutputsize(prefix + "." + pair.getKey())));
	        	}	        	
	        	
	        }
    
	        it.remove(); // avoids a ConcurrentModificationException
	    }
	}
	
	abstract protected BigDecimal getHighSeverityCode() throws PcmlException;
	
	abstract protected RPGErrorResponse getErrorMessages(String program) throws PcmlException, RPGException;
		
    abstract protected void verifyRPGSeverity(RPGRequest request, BigDecimal highestMessageSeverity) throws RPGException, PcmlException;
    
}
