package net.bglgroup.spring.common.rpgcallprogram;

import com.ibm.as400.data.PcmlException;

import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;

public interface Program 
{
	public void createPCMLDocument(RPGRequest request) throws PcmlException, RPGException;
	public void callProgram(RPGRequest request) throws RPGException;
	public Object getResponseValue(String fieldName) throws RPGException;
	public Object getArrayResponseValue(String fieldName, int index) throws RPGException;
	
}
