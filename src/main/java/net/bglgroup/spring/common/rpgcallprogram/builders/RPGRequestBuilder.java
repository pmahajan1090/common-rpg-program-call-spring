package net.bglgroup.spring.common.rpgcallprogram.builders;

import java.util.HashMap;
import java.util.Map;

import net.bglgroup.spring.common.rpgcallprogram.connectionpool.ConnectionDetails;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;

public final class RPGRequestBuilder 
{    
	private Long                requestTime = System.currentTimeMillis();
    private ConnectionDetails   connectionDetails;
    private String 			    programName = "LDI6038";
    private Map<String, Object> headers     = new HashMap<String, Object>();
    private Map<String, Object> body        = new HashMap<String, Object>();
 
    public RPGRequestBuilder setConnectionDetails(ConnectionDetails connectionDetails) 
    {
    	this.connectionDetails = connectionDetails;
    	
    	return this;
    }
    
    public RPGRequestBuilder addHeader(String name, Object value)
    {
    	this.headers.put(name, value);
    	
    	return this;
    }
    
    public RPGRequestBuilder addBody(String name, Object value)
    {
    	this.body.put(name, value);
    	
    	return this;
    }
    
    public RPGRequestBuilder setHeaders(Map<String, Object> headers)
    {
    	this.headers = headers;
    	
    	return this;
    }
    
    public RPGRequestBuilder setBody(Map<String, Object> body)
    {
    	this.body = body;
    	
    	return this;
    }
    
    public RPGRequestBuilder setProgramName(String programName)
    {
    	this.programName = programName;
    	
    	return this;
    }
    
    public RPGRequestBuilder setRequestTime(Long requestTime)
    {
    	this.requestTime = requestTime;
    	
    	return this;
    }
    
    public RPGRequest build()
    {			
    	return new RPGRequest(requestTime, connectionDetails, programName, headers, body);
    }
}
