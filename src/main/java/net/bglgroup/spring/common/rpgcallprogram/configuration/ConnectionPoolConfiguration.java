package net.bglgroup.spring.common.rpgcallprogram.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ibm.as400.access.AS400ConnectionPool;

@Configuration
public class ConnectionPoolConfiguration 
{			
	@Value("${iseries.as400.maxconnections}")
    private int maxConnections;
	
	@Value("${iseries.as400.maxusetime.millis}")
    private Long maxUseTime;
	
	@Value("${iseries.as400.maxinactivity.millis}")
    private Long maxInactivity;
	
	@Value("${iseries.as400.cleanupinterval.millis}")
    private Long cleanupInterval;
	

	@Bean
    public AS400ConnectionPool connectionPoolSetup() 
    {  
        return connectionPoolSetup(maxConnections, maxUseTime, maxInactivity, cleanupInterval);
    }
		
	private AS400ConnectionPool connectionPoolSetup(int maxConnections, Long maxUseTime, Long maxInactivity, Long cleanupInterval) 
    {
        AS400ConnectionPool connectionPool = new AS400ConnectionPool();

        connectionPool.setMaxConnections(maxConnections);
        connectionPool.setMaxUseTime(maxUseTime);
        connectionPool.setMaxInactivity(maxInactivity);
        connectionPool.setCleanupInterval(cleanupInterval);
        
        return connectionPool;
    }
}
