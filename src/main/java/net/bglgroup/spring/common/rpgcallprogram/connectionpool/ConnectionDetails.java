package net.bglgroup.spring.common.rpgcallprogram.connectionpool;

import lombok.Getter;

@Getter
public final class ConnectionDetails
{
	private String hostName;
    private String username;
    private String password;
    
	public ConnectionDetails(String hostName, String username, String password) 
	{
		this.hostName = hostName;
		this.username = username;
		this.password = password;
	}
}
