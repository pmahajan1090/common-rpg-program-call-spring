package net.bglgroup.spring.common.rpgcallprogram.connectionpool;

import com.ibm.as400.access.AS400;

import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;

public interface ConnectionPool 
{
	public AS400 getConnection(RPGRequest request) throws RPGException;
	public void releaseConnection(RPGRequest request, AS400 as400);
}
