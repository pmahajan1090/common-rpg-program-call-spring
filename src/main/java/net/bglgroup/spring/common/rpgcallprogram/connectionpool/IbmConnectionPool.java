package net.bglgroup.spring.common.rpgcallprogram.connectionpool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400ConnectionPool;
import com.ibm.as400.access.ConnectionPoolException;

import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;


@Component
public class IbmConnectionPool implements ConnectionPool
{		
	@Autowired
	private AS400ConnectionPool connectionPoolSetup;
		
	
	public AS400 getConnection(RPGRequest request) throws RPGException
	{
        AS400 as400 = null;    
			        
	    try 
	    { //contain these in the RPGRequest instead.
	    	as400 = connectionPoolSetup.getConnection(request.getConnectionDetails().getHostName(), 
	    			                                  request.getConnectionDetails().getUsername(),
	    			                                  request.getConnectionDetails().getPassword(), AS400.COMMAND);
		} 
	    catch (ConnectionPoolException e) 
	    {
	    	throw new RPGException(request, e.getMessage());
		}
		    
		return as400;
	}
	
	public void releaseConnection(RPGRequest request, AS400 as400)
	{
		connectionPoolSetup.returnConnectionToPool(as400);
	}

}
