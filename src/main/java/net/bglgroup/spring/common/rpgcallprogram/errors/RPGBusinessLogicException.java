package net.bglgroup.spring.common.rpgcallprogram.errors;

import lombok.Getter;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;

@Getter
public final class RPGBusinessLogicException extends RPGSeverityException
{
	private static final long serialVersionUID = 1L;
	private RPGErrorResponse errorMessages;
	
	public RPGBusinessLogicException(RPGRequest rpgRequest, int severityCode, RPGErrorResponse errorMessages) 
	{
		super(rpgRequest, severityCode, null);
		this.errorMessages = errorMessages;
	}
}
