package net.bglgroup.spring.common.rpgcallprogram.errors;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class RPGErrorMessage 
{
	private String code;
	private String description;
	
	public RPGErrorMessage(String code, String description)
	{
		this.setCode(code);
		this.setDescription(description);
	}
}
