package net.bglgroup.spring.common.rpgcallprogram.errors;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class RPGErrorResponse 
{
	private ArrayList<RPGErrorMessage> messages;
	
	public RPGErrorResponse(ArrayList<RPGErrorMessage> messages)
	{
		this.setMessages(messages);
	}
}
