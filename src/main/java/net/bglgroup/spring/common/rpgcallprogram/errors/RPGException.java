package net.bglgroup.spring.common.rpgcallprogram.errors;

import lombok.Getter;
import lombok.Setter;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;


@Getter
@Setter
public class RPGException extends Exception 
{
	private static final long serialVersionUID = 1L;
	private RPGRequest rpgRequest;
	private String message;
	
	public RPGException(RPGRequest rpgRequest, String errorMessage)
	{
		this.setRpgRequest(rpgRequest);
		this.setMessage(errorMessage);		
	}
}
