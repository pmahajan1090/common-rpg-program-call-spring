package net.bglgroup.spring.common.rpgcallprogram.errors;

import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;

public final class RPGInternalServerException extends RPGSeverityException
{
	private static final long serialVersionUID = 1L;

	public RPGInternalServerException(RPGRequest rpgRequest, int severityCode, String errorMessage) 
	{
		super(rpgRequest, severityCode, errorMessage);

		this.setSeverityCode(severityCode);
	}
}
