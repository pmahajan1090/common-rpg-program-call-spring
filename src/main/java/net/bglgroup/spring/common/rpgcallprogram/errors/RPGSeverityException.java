package net.bglgroup.spring.common.rpgcallprogram.errors;

import lombok.Getter;
import lombok.Setter;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;

@Getter
@Setter
public class RPGSeverityException extends RPGException
{
	private static final long serialVersionUID = 1L;
	private int severityCode;

	public RPGSeverityException(RPGRequest rpgRequest, int severityCode, String errorMessage) 
	{
		super(rpgRequest, errorMessage);

		this.setSeverityCode(severityCode);
	}
}
