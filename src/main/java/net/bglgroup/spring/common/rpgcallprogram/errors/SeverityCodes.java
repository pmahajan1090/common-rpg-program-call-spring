package net.bglgroup.spring.common.rpgcallprogram.errors;

import java.util.*;

public enum SeverityCodes 
{				
	SEVERITY_0(0),		// (00 = Information) & log level
	SEVERITY_10(10),	// (10 = Warning) & log level
	SEVERITY_13(13),	// (13 = Accepted Batch Process is in progress)
	SEVERITY_15(15),	// (15 = Business error)
	SEVERITY_20(20),	// (20 = Error) & log level
	SEVERITY_30(30),	// (30 = Severe error)
	SEVERITY_40(40),	// (40 = Abnormal end of program/function)
	SEVERITY_50(50);	// (50 = Abnormal end of job);
	
	private int severity;

	private SeverityCodes(Integer severity) 
	{
		this.severity = severity;
	}

	public int getSeverity() 
	{
		return severity;
	}
	
	public static SeverityCodes getBySeverity(final int severity)
	{
	    return Arrays.stream(SeverityCodes.values()).filter(severityCode -> severityCode.getSeverity() == severity).findFirst().orElse(null);
	}
}
