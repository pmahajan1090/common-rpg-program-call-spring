package net.bglgroup.spring.common.rpgcallprogram.requests;

import java.util.Map;

import lombok.Getter;
import net.bglgroup.spring.common.rpgcallprogram.connectionpool.ConnectionDetails;

@Getter
public final class RPGRequest
{
	private String programName;
	private String programExtension = ".pgm";
	private String pcmlFilePath = "pcml/";
	private Map<String, Object> headers;
	private Map<String, Object> body;
	private Long requestTime = System.currentTimeMillis();
	private ConnectionDetails connectionDetails;
		
	public RPGRequest(Long requestTime, ConnectionDetails connectionDetails, String programName, Map<String, Object> headers, Map<String, Object> body)
	{
		this(connectionDetails, programName, headers, body);
		this.requestTime = requestTime;
	}
	
	public RPGRequest(ConnectionDetails connectionDetails, String programName, Map<String, Object> headers, Map<String, Object> body)
	{
		this.connectionDetails = connectionDetails;
		this.programName = programName;
		this.headers = headers;
		this.body = body;
	}
}
