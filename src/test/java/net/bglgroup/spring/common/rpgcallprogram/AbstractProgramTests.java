package net.bglgroup.spring.common.rpgcallprogram;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.spy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.ibm.as400.access.AS400;
import com.ibm.as400.data.PcmlException;
import com.ibm.as400.data.ProgramCallDocument;

import net.bglgroup.spring.common.rpgcallprogram.builders.RPGRequestBuilder;
import net.bglgroup.spring.common.rpgcallprogram.connectionpool.ConnectionPool;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGBusinessLogicException;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGInternalServerException;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGNotFoundException;
import net.bglgroup.spring.common.rpgcallprogram.errors.SeverityCodes;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;
import net.bglgroup.spring.common.rpgcallprogram.testclasses.RPGProgram;

import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class AbstractProgramTests 
{						
	@Mock
	private ProgramCallDocument programCallDocument;
					
	@Mock
	private ConnectionPool connectionPool;
	
	@InjectMocks
	private RPGProgram rpgProgram = spy(RPGProgram.class);
	
	private RPGRequest request;
	
	@BeforeEach
    public void initObjects() throws PcmlException, RPGException
	{		
		var claimsArrayList = new ArrayList<HashMap<String, Object>>();			
		
		for (int i=0; i < 5; i++)
		{				
			var claimsHashMap = new HashMap<String, Object>();
			claimsHashMap.put("CLAIMNUMBER", 		"number"+i);
			claimsHashMap.put("CLAIMDATE", 			20220101);
			claimsHashMap.put("CLAIMSTATUS", 		null);
			claimsHashMap.put("CAUSEOFLOSS", 		10);
			claimsHashMap.put("NCDINDICATOR", 		"I");
			claimsHashMap.put("CLAIMPAYMENTTOTAL", 	100.00);
			claimsHashMap.put("SUBJECTPAYMENT", 	50.00);
			claimsHashMap.put("IIN", 				"iIN");
			claimsHashMap.put("INSURERCONTACT", 	"insurerContact");
			claimsHashMap.put("SUBJECTTYPECODE", 	"ST");
			claimsHashMap.put("FAULTSTATUS", 		"faultStatus");
			claimsHashMap.put("DLN", 				"dLN");
			claimsHashMap.put("VRN", 				"vRN");
			
			claimsArrayList.add(claimsHashMap);
		}
							    
		request = new RPGRequestBuilder().setProgramName("TEST")
				.addHeader("XTRACEID", "trace-id")
                .addHeader("XREQUESTID", "request-id")
                .addHeader("XMESSAGEID", "message-id")
                .addHeader("AMENDMENTTRANSACTIONID", "1")
                .addHeader("CUSTOMERACCOUNTID", "123456789")
                .addBody("CLAIMSUMMARY.REQUESTID", new BigDecimal("220310145858567196731001"))
                .addBody("CLAIMSUMMARY.REFERENCETYPE", "CUE")
                .addBody("CLAIMSUMMARY.REFERENCENUMBER", "133272176")
                .addBody("CLAIMSUMMARY.PERSONID", 01)
                .addBody("CLAIMSUMMARY.EXPERIANRETURNCODE", "0")
                .addBody("CLAIMSUMMARY.PREVIOUSDECLAREDMIN", 0)
                .addBody("CLAIMSUMMARY.PREVIOUSDECLAREDMAX", 0)
                .addBody("CLAIMSUMMARY.PREVIOUSDECLAREDLATEST", 0)
                .addBody("CLAIMSUMMARY.RETURNEDCLAIMSCOUNT", 5)
                .addBody("CLAIMSUMMARY.DISCLOSEDCLAIMSINDICATOR", "I")
                .addBody("CLAIMSCOUNT", 5)
                .addBody("CLAIMS", claimsArrayList).build();	
					
		
		

		rpgProgram.setRPGRequest(request);
		
		//Mock Connections
		AS400 as400 = new AS400();
		Mockito.when(rpgProgram.getAs400Connection(request)).thenReturn(as400); 
		Mockito.doNothing().when(rpgProgram).setAs400(as400);		
				
		//Mock successful call to service
		Mockito.when(programCallDocument.callProgram(request.getProgramName())).thenReturn(true);	
	}
		
	@Test
	public void pcmlDocumentTest() 
	{
		assertDoesNotThrow(()-> {

			rpgProgram.createPCMLDocument(request);
		
		});
	}
	
	@Test
	public void exceptionTest()
	{
		assertThrows(RPGBusinessLogicException.class, () -> {
			rpgProgram.verifyRPGSeverity(request, new BigDecimal(SeverityCodes.SEVERITY_15.getSeverity()));
         });
		
		assertThrows(RPGNotFoundException.class, () -> {
			rpgProgram.verifyRPGSeverity(request, new BigDecimal(SeverityCodes.SEVERITY_20.getSeverity()));
         });
		
		assertThrows(RPGInternalServerException.class, () -> {
			rpgProgram.verifyRPGSeverity(request, new BigDecimal(SeverityCodes.SEVERITY_30.getSeverity()));
         });		       
	
	}
	
	@Test
	public void responseTest()
	{
		assertDoesNotThrow(()-> {
			Mockito.when(programCallDocument.getValue(request.getProgramName() + "." + "testField")).thenReturn("testValue");	
					
			String responseValue = (String) rpgProgram.getResponseValue("testField");
			
			assertTrue(responseValue.equals("testValue"));
		});
	}
	
	@Test
	public void arrayResponseTest()
	{
		assertDoesNotThrow(()-> {
			
			int indices[] = new int[1];
	       	indices[0] = 0; 
	       	
			Mockito.when(rpgProgram.getIndices(0)).thenReturn(indices);
			Mockito.when(programCallDocument.getValue(request.getProgramName() + "." + "testField", indices)).thenReturn("testValue");	
					
			String responseValue = (String) rpgProgram.getArrayResponseValue("testField", 0);
			
			assertTrue(responseValue.equals("testValue"));
		});
	}
	
	@Test
	public void callProgramTest()
	{		
		assertDoesNotThrow(()-> {
			
			Mockito.doNothing().when(rpgProgram).createPCMLDocument(request);
			Mockito.when(rpgProgram.getHighSeverityCode()).thenReturn(BigDecimal.ZERO);
			
		    rpgProgram.callProgram(request);
			
		});
	}
}