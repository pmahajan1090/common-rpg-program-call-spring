package net.bglgroup.spring.common.rpgcallprogram.connectionpool;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400ConnectionPool;

import net.bglgroup.spring.common.rpgcallprogram.builders.RPGRequestBuilder;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;

@ExtendWith(SpringExtension.class)
public class IbmConnectionPoolTests
{

	@Mock
	private AS400ConnectionPool connectionPoolSetup;
		
	@InjectMocks
	private IbmConnectionPool connectionPool = new IbmConnectionPool();

	
	@Test
	public void getSingleConnectionTest() throws RPGException 
	{
		RPGRequest request = new RPGRequestBuilder().setRequestTime(Long.valueOf(10)).setProgramName("TEST")
				                                .setConnectionDetails(new ConnectionDetails("YOGI", "username", "password"))
								                .addHeader("XTRACEID", "trace-id")
								                .addHeader("XREQUESTID", "request-id")
								                .addHeader("XMESSAGEID", "message-id")
								                .addHeader("AMENDMENTTRANSACTIONID", "1")
								                .addHeader("CUSTOMERACCOUNTID", "123456789")
								                .addBody("UPDATEADDRESS.ID", "123456789-R")
								                .addBody("UPDATEADDRESS.LINE1", "line1")
								                .addBody("UPDATEADDRESS.LINE2", "line2")
								                .addBody("UPDATEADDRESS.LINE3", "line3")
								                .addBody("UPDATEADDRESS.LINE4", "line4")
								                .addBody("UPDATEADDRESS.LINE5", "line5")
								                .addBody("UPDATEADDRESS.POSTCODE", "postcode").build();
		
		AS400 as400Connection = connectionPool.getConnection(request);
		connectionPool.releaseConnection(request, as400Connection);
	}

}
