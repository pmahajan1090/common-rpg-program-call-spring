package net.bglgroup.spring.common.rpgcallprogram.testclasses;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.ibm.as400.data.PcmlException;

import net.bglgroup.spring.common.rpgcallprogram.AbstractProgram;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGBusinessLogicException;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGErrorMessage;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGErrorResponse;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGInternalServerException;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGNotFoundException;
import net.bglgroup.spring.common.rpgcallprogram.errors.SeverityCodes;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;

public class RPGProgram extends AbstractProgram
{

	@Override
	public BigDecimal getHighSeverityCode() throws PcmlException {
		return BigDecimal.ZERO;
	}

	@Override
	protected RPGErrorResponse getErrorMessages(String program) throws PcmlException, RPGException {

		var messages = new ArrayList<RPGErrorMessage>();
		messages.add(new RPGErrorMessage("BGL100", "Error"));
		
		return new RPGErrorResponse(messages);
	}
	
	@Override
    public void verifyRPGSeverity(RPGRequest request, BigDecimal highestMessageSeverity) throws RPGException, PcmlException 
    {    			
		switch (SeverityCodes.getBySeverity(highestMessageSeverity.intValue())) 
		{
		    case SEVERITY_0:
		    case SEVERITY_10:
		    case SEVERITY_13:
			    break;  // Successful statuses to be ignored.
			    
		    case SEVERITY_15:
				throw new RPGBusinessLogicException(request, highestMessageSeverity.intValue(), getErrorMessages(request.getProgramName()));
				
			case SEVERITY_20:				
				throw new RPGNotFoundException(request, highestMessageSeverity.intValue(), "Error");

			default:  // 500 exceptions for severities (30,40,50) and any unexpected
				throw new RPGInternalServerException(request, highestMessageSeverity.intValue(), "Error");
		}		
   }

}
